package com.intevalue.exchange.service.impl;

import java.io.IOException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.intevalue.exchange.api.call.BaseClient;
import com.intevalue.exchange.dao.ExchangeRepository;
import com.intevalue.exchange.entity.Exchange;
import com.intevalue.exchange.service.BaseService;
import com.intevalue.exchange.util.Constant;

@Service
public class Service1 extends BaseService {
	
	@Autowired
	private ExchangeRepository exchangeRepository;
	
	@Autowired
	private BaseClient client;
	
	@Value("${rates.url}")
	private String url;

	@Override
	public String getCountryRate() {
		
		String result = "";
		try {
			result = client.getResponse(getUrl(), Constant.GET, "");
			
			JSONObject json = new JSONObject(result);
			JSONObject rates = json.getJSONObject("rates");
			
			rates.keySet().forEach(key ->
		    {
		        Object value = rates.get(key);
		        Exchange exchange = exchangeRepository.findByCountryCode(key);
		        if (exchange != null) {
		        	exchange.setRate(Double.parseDouble(value.toString()));
					exchangeRepository.save(exchange);
				} else {		
					exchange = new Exchange();
					exchange.setCountryCode(key);
					exchange.setRate(Double.parseDouble(value.toString()));
					exchangeRepository.save(exchange);
				}
		    });

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getUrl() {
		return url;
	}

}
