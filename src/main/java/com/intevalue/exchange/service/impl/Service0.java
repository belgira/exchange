package com.intevalue.exchange.service.impl;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.intevalue.exchange.service.BaseService;

@Service
public class Service0 extends BaseService {

	@Override
	public String getCountryRate() {
		JSONObject json = new JSONObject();
		JSONObject jsonValue = new JSONObject();
		jsonValue.put("PHP", "1.0");
		json.put("rates", jsonValue);
		return json.toString();
	}

}
