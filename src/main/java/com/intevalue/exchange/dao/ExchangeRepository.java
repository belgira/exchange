package com.intevalue.exchange.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.intevalue.exchange.entity.Exchange;

@Repository
public interface ExchangeRepository extends CrudRepository<Exchange, Integer> {
	
	Exchange findByCountryCode(String countryCode);

}
