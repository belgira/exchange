package com.intevalue.exchange.api.call;

import java.io.IOException;

public abstract class BaseClient {
	
	public abstract String getResponse(String url, String method, String json) throws IOException;
	
}
