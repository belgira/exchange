package com.intevalue.exchange.api.call;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.intevalue.exchange.util.Constant;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Component
public class Client extends BaseClient {

	public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

	@Override
	public String getResponse(String url, String method, String json) throws IOException {
		OkHttpClient client = new OkHttpClient();
		RequestBody body = RequestBody.create(JSON, json);
		Request request = null;

		if (method.equalsIgnoreCase(Constant.GET)) {
			request = new Request.Builder().url(url).get().build();
		} else {
			request = new Request.Builder().url(url).post(body).build();
		}
		
		try (Response response = client.newCall(request).execute()) {
			return response.body().string();
		}
	}
}
