package com.intevalue.exchange.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.intevalue.exchange.service.BaseService;

@RestController	
public class ExchangeController {
	
	@Autowired
	private ApplicationContext context;
	
	@GetMapping(value = "/rate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCountryRate(@RequestParam String version) {
		BaseService service = (BaseService) context.getBean("service" + version);		
		String result = service.getCountryRate();
		return ResponseEntity.ok(result);      
    }

}
